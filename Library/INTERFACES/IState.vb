''' <summary>
''' Interface to a finite state machine state.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/26/09" by="David" revision="2.3.3403.x">
''' created
''' </history>
Public Interface IState

    Inherits IDisposable

#Region " MEMBERS "

    ''' <summary>
    ''' Gets the state name.
    ''' The state name must be unique for the state machine.
    ''' </summary>
    ReadOnly Property Name() As String

    ''' <summary>
    ''' Gets or sets the state caption to use when displaying the state.
    ''' Initialized to the state name.
    ''' </summary>
    Property Caption() As String

    ''' <summary>
    ''' Gets or sets the number of times the state can be re-entered consecutively. 
    ''' </summary>
    Property CountOutCount() As Integer

    ''' <summary>
    ''' Returns true if the number of consecutive re-entries exceeded the count out interval. 
    ''' </summary>
    Function IsCountOut() As Boolean

    ''' <summary>
    ''' Gets the sentinel indicating if the state hit timeout. 
    ''' </summary>
    ReadOnly Property IsTimeout() As Boolean

    ''' <summary>
    ''' Gets or sets the state timeout interval in milliseconds. 
    ''' </summary>
    Property TimeoutInterval() As Integer

    ''' <summary>
    ''' Gets or sets the state unique ID. 
    ''' Initialized to the state index + 1 in the collection of states. 
    ''' </summary>
    Property UniqueId() As Integer

#End Region

#Region " EVENTS "

    ''' <summary>Occurs upon state actions.</summary>
    ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
    Event Act As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Act">Act</see> event.</summary>
    Sub OnAct(ByVal e As System.EventArgs)

    ''' <summary>Occurs upon state activation (entry).</summary>
    ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
    Event Enter As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the <see cref="Enter">Enter</see> event.</summary>
    Sub OnEnter(ByVal e As System.EventArgs)

    ''' <summary>Occurs upon state deactivation (exit).</summary>
    ''' <remarks>Use this event to notify the container class that the state was deactivated.</remarks>
    Event Leave As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>Raises the <see cref="Leave">Leave</see> event.</summary>
    Sub OnLeave(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary>Occurs if state operations failed to complete before the timeout timer
    ''' fired.</summary>
    ''' <remarks>Use this event to process timeout operations.</remarks>
    Event Timeout As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Handles timeout events.
    ''' </summary>
    Sub OnTimeout(ByVal e As System.EventArgs)

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Implements comparison with a generic object.
    ''' </summary>
    Function Equals(ByVal value As Object) As Boolean

    ''' <summary>
    ''' Implements comparison with another state.
    ''' </summary>
    Function Equals(ByVal value As IState) As Boolean

    ''' <summary>
    ''' Returns the hash code for the state.
    ''' </summary>
    Function GetHashCode() As Integer

#End Region

End Interface
