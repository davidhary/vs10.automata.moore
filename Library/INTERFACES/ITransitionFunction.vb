''' <summary>
''' Interface to a finite state machine transition function.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/26/09" by="David" revision="2.3.3403.x">
''' created
''' </history>
Public Interface ITransitionFunction(Of T)

    Inherits IDisposable

#Region " TRANSITION TABLE CONSTRUCTION AND ANALYSIS "

    ''' <summary>
    ''' Returns the analysis report.
    ''' </summary>
    ReadOnly Property AnalysisReport() As String

    ''' <summary>
    ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
    ''' next state. Namely, if all transitions are unique.
    ''' </summary>
    ReadOnly Property IsDeterministic() As Boolean

    ''' <summary>
    ''' Adds a translation to the transition table.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    ''' <param name="nextState">Specifies the next state.</param>
    Function AddTransition(ByVal state As IState, ByVal symbol As T, ByVal nextState As IState) As ITransition(Of T)

#End Region

#Region " INPUT SYMBOL PROCESSING "

    ''' <summary>
    ''' Traverses the transition function and transition specified for the current state and the 
    ''' input symbol.
    ''' </summary>
    ''' <param name="state">Specifies the current state.</param>
    ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
    Function SelectTransition(ByVal state As IState, ByVal symbol As T) As ITransition(Of T)

#End Region

End Interface
