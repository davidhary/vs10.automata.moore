
''' <summary>
''' Interface to a finite state machine transition.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/26/09" by="David" revision="2.3.3403.x">
''' created
''' </history>
Public Interface ITransition(Of T)
    Inherits IDisposable

#Region " MEMBERS "

    ''' <summary>
    ''' Gets or sets reference to the current <see cref="IState">state</see> for this <see cref="ITransition">transition</see> 
    ''' </summary>
    ReadOnly Property State() As IState

    ''' <summary>
    ''' Gets or sets the symbol for transitioning to the next state.
    ''' </summary>
    ReadOnly Property Symbol() As T

    ''' <summary>
    ''' Gets or sets reference to the next current <see cref="IState">state</see> for this <see cref="ITransition">transition</see> 
    ''' </summary>
    ReadOnly Property NextState() As IState

#End Region

#Region " EVENTS "

    ''' <summary>Occurs before the state transition.</summary>
    ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
    Event Starting As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>Occurs after the state transition.</summary>
    ''' <remarks>Use this event to notify the container class that the state actions are taking place.</remarks>
    Event Completed As EventHandler(Of System.EventArgs)

#End Region

#Region " ON EVENT HANDLERS "

    ''' <summary>Raises the <see cref="Starting">Starting</see> event.</summary>
    Sub OnStarting(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary>Raises the <see cref="Completed">Completed</see> event.</summary>
    Sub OnCompleted(ByVal e As System.EventArgs)

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Compares the <see cref="ITransition(Of T)">transition</see> to a generic object.
    ''' </summary>
    Function Equals(ByVal value As Object) As Boolean

    ''' <summary>
    ''' Compares to another <see cref="ITransition(Of T)">transition</see>.
    ''' Transitions are compared using only the current state and symbol as 
    ''' this state machine must be deterministic.
    ''' </summary>
    Function Equals(ByVal value As ITransition(Of T)) As Boolean

    ''' <summary>
    ''' Returns a unique hash code for this transition.
    ''' </summary>
    Function GetHashCode() As Integer

#End Region

End Interface

