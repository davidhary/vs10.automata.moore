''' <summary>
''' Defines event arguments accepting the generic symbol for the state machine.
''' </summary>
''' <license>
''' http://www.CodeProject.com/KB/cs/GenericEventArgs.aspx<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="05/04/2009" by="Stewart and StageSoft" revision="1.2.3411.x">
''' created
''' </history>
Public Class SymbolEventArgs(Of T)
    Inherits EventArgs

    Public Sub New(ByVal value As T)
        MyBase.New()
        Me._symbol = value
    End Sub

    ''' <summary>
    ''' Gets or sets the symbol.
    ''' </summary>
    ''' <value>The symbol.</value>
    Public Property Symbol() As T

End Class

