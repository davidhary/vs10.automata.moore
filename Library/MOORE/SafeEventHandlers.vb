﻿Friend Module SafeEventHandlers

    ''' <summary> Safely triggers an event. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                              ByVal sender As Object, ByVal e As TEventArgs)
        If Not handler Is Nothing Then
            For Each d As [Delegate] In handler.GetInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                    If target IsNot Nothing AndAlso target.InvokeRequired Then
                        Dim result As IAsyncResult = target.BeginInvoke(handler, New Object() {sender, e})
                        If result IsNot Nothing Then
                            target.EndInvoke(result)
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                End If
            Next

        End If
    End Sub

End Module

#Region " UNUSED "
#If False Then
        ''' <summary> Safely triggers an event. </summary>
        ''' <remarks> Safe Begin End Invoke Benchmarked at 0.054185ms '''. </remarks>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <System.Runtime.CompilerServices.Extension()>
        Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            If Not value Is Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Try
                                Dim result As IAsyncResult = target.BeginInvoke(value, New Object() {sender, e})
                                If result IsNot Nothing Then
                                    target.EndInvoke(result)
                                End If
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next

            End If
        End Sub


#End If
#End Region