Imports System.Collections.Generic

Namespace Moore

    ''' <summary>
    ''' Defines the state transition function or table.
    ''' </summary>
    ''' <remarks>
    ''' The transition tables defines how a state machine transits between states..
    ''' </remarks>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/26/09" by="David" revision="2.3.3403.x">
    ''' based on Moore machine in C# by Alexander Muller from http://www.CodeProject.com/KB/recipes/MooreMachine.aspx
    ''' </history>
    <ComponentModel.Description("Finite State Machine Transition Function")>
    Public Class TransitionFunction(Of T)

        Implements ITransitionFunction(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()
            Me._transitions = New LinkedList(Of ITransition(Of T))()
        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._transitions IsNot Nothing Then
                            For Each t As Transition(Of T) In Me._transitions
                                If t IsNot Nothing Then
                                    t.Dispose()
                                    t = Nothing
                                End If
                            Next
                            Me._transitions.Clear()
                            Me._transitions = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " TRANSITION TABLE CONSTRUCTION AND ANALYSIS "

        Private _analysisReport As System.Text.StringBuilder
        ''' <summary>
        ''' Returns the analysis report.
        ''' </summary>
        Public ReadOnly Property AnalysisReport() As String Implements ITransitionFunction(Of T).AnalysisReport
            Get
                If Me._analysisReport Is Nothing Then
                    Return "Analysis not run"
                Else
                    Return Me._analysisReport.ToString
                End If
            End Get
        End Property

        Private _isDeterministic As Boolean
        ''' <summary>
        ''' Returns true if the state machine is deterministic, i.e., if each state and symbol have a unique
        ''' next state. Namely, if all transitions are unique.
        ''' </summary>
        Public ReadOnly Property IsDeterministic() As Boolean Implements ITransitionFunction(Of T).IsDeterministic
            Get
                Return Me._isDeterministic
            End Get
        End Property

        ''' <summary>
        ''' Gets or sets the linked-list of transitions.
        ''' </summary>
        Private _transitions As LinkedList(Of ITransition(Of T))

        ''' <summary>
        ''' Adds a transition to the transition table.
        ''' </summary>
        ''' <param name="state">Specifies the current state.</param>
        ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
        ''' <param name="nextState">Specifies the next state.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
        Justification:="Object is returned")>
        Public Function AddTransition(ByVal state As IState, ByVal symbol As T, ByVal nextState As IState) As ITransition(Of T) Implements ITransitionFunction(Of T).AddTransition

            Me._analysisReport = New System.Text.StringBuilder
            Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                            "Deterministic analysis report {1:DD-MM-yyyy HH:mm:ss}{0}", Environment.NewLine, DateTime.Now)

            Me._isDeterministic = True
            Dim candidateTransition As ITransition(Of T)
            candidateTransition = New Transition(Of T)(state, symbol, nextState)

            If Me._transitions.Count > 1 Then

                Dim node As LinkedListNode(Of ITransition(Of T)) = Me._transitions.First
                Do While node IsNot Nothing

                    If node.Value.Equals(candidateTransition) Then
                        Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                        "New Transition from State '{1}' with Symbol '{3}' to State '{2}' conflicts with transition {0}", Environment.NewLine,
                                                        candidateTransition.State.Name, candidateTransition.Symbol.ToString, candidateTransition.NextState.Name)
                        Me._analysisReport.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                        "               from State '{1}' with Symbol '{2}' to State '{3}'{0}", Environment.NewLine,
                                                        node.Value.State.Name, node.Value.Symbol.ToString, node.Value.NextState.Name)
                        Me._isDeterministic = False
                    End If
                    node = node.Next
                Loop
            End If

            If Me._isDeterministic Then
                Me._analysisReport.Append("Transition function is deterministic.")
            End If

            Me._transitions.AddLast(candidateTransition)

            Return candidateTransition

        End Function

#End Region

#Region " INPUT SYMBOL PROCESSING "

        ''' <summary>
        ''' Traverses the transition function and transition specified for the current state and the 
        ''' input symbol.
        ''' </summary>
        ''' <param name="state">Specifies the current state.</param>
        ''' <param name="symbol">Specifies the input symbol that signals the transition.</param>
        Friend Function SelectTransition(ByVal state As IState, ByVal symbol As T) As ITransition(Of T) Implements ITransitionFunction(Of T).SelectTransition

            Dim node As LinkedListNode(Of ITransition(Of T)) = Me._transitions.First

            Do While node IsNot Nothing

                Dim t As ITransition(Of T) = node.Value

                If t.State.Equals(state) AndAlso t.Symbol.Equals(symbol) Then

                    Return t

                End If

                node = node.Next

            Loop

            Return New Transition(Of T)(state, symbol, Nothing)

        End Function


#End Region

    End Class

End Namespace
