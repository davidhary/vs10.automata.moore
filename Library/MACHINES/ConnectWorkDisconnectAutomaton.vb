Imports System.ComponentModel

Namespace Moore

    ''' <summary>
    ''' A base automaton class for implementing a finite automata for opening, working, and closing constructs.
    ''' </summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/26/09" by="David" revision="2.3.3403.x">
    ''' created
    ''' </history>
    Public Class ConnectWorkDisconnectAutomaton
        Inherits Machine(Of ConnectWorkDisconnectSymbol)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()
            MyBase.New()

            eventLocker = New Object
            MyBase.OnsetDelay = 500
            MyBase.Interval = 1000

            ' define the states.
            Me._idleState = MyBase.AddState("IDLE")
            Me._failureState = MyBase.AddState("FAILURE")
            Me._failureState.CountOutCount = 3
            Me._connectingState = MyBase.AddState("CONNECTING")
            Me._connectedState = MyBase.AddState("CONNECTED")
            Me._workingState = MyBase.AddState("WORKING")
            Me._workDoneState = MyBase.AddState("WORK_DONE")
            Me._disconnectingState = MyBase.AddState("DISCONNECTING")
            Me._disconnectedState = MyBase.AddState("DISCONNECTED")

            ' set the start state
            Me.StartStateSetter(Me._idleState)

            ' define the signals.
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.Failure)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.Idle)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.StartConnecting)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.FinishConnecting)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.StartWorking)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.FinishWorking)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.StartDisconnecting)
            MyBase.AddSymbol(ConnectWorkDisconnectSymbol.FinishDisconnecting)

            ' define the transitions

            ' IDLE TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, ConnectWorkDisconnectSymbol.Idle, Me._idleState)
            Next

            ' FAILURE TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, ConnectWorkDisconnectSymbol.Failure, Me._failureState)
            Next

            MyBase.AddTransition(Me._idleState, ConnectWorkDisconnectSymbol.StartConnecting, Me._connectingState)

            MyBase.AddTransition(Me._connectingState, ConnectWorkDisconnectSymbol.FinishConnecting, Me._connectedState)
            MyBase.AddTransition(Me._connectingState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._disconnectingState)

            MyBase.AddTransition(Me._connectedState, ConnectWorkDisconnectSymbol.StartWorking, Me._workingState)
            MyBase.AddTransition(Me._connectedState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._disconnectingState)

            MyBase.AddTransition(Me._workingState, ConnectWorkDisconnectSymbol.FinishWorking, Me._workDoneState)
            MyBase.AddTransition(Me._workingState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._disconnectingState)

            MyBase.AddTransition(Me._workDoneState, ConnectWorkDisconnectSymbol.StartWorking, Me._workingState)
            MyBase.AddTransition(Me._workDoneState, ConnectWorkDisconnectSymbol.StartDisconnecting, Me._disconnectingState)

            MyBase.AddTransition(Me._disconnectingState, ConnectWorkDisconnectSymbol.FinishDisconnecting, Me._disconnectedState)

            MyBase.AddTransition(Me._disconnectedState, ConnectWorkDisconnectSymbol.StartConnecting, Me._connectingState)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        If Me.ConnectedEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.ConnectedEvent.GetInvocationList
                                RemoveHandler Me.Connected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.ConnectingEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.ConnectingEvent.GetInvocationList
                                RemoveHandler Me.Connecting, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.DisconnectedEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.DisconnectedEvent.GetInvocationList
                                RemoveHandler Me.Disconnected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.DisconnectingEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.DisconnectingEvent.GetInvocationList
                                RemoveHandler Me.Disconnecting, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.FailureEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.FailureEvent.GetInvocationList
                                RemoveHandler Me.Failure, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.ReadyEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.ReadyEvent.GetInvocationList
                                RemoveHandler Me.Ready, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.WorkDoneEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.WorkDoneEvent.GetInvocationList
                                RemoveHandler Me.WorkDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If
                        If Me.WorkingEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.WorkingEvent.GetInvocationList
                                RemoveHandler Me.Working, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        ' Free managed resources when explicitly called
                        If Me._idleState IsNot Nothing Then
                            Me._idleState = Nothing
                        End If
                        If Me._connectingState IsNot Nothing Then
                            Me._connectingState = Nothing
                        End If
                        If Me._connectedState IsNot Nothing Then
                            Me._connectedState = Nothing
                        End If
                        If Me._workingState IsNot Nothing Then
                            Me._workingState = Nothing
                        End If
                        If Me._workDoneState IsNot Nothing Then
                            Me._workDoneState = Nothing
                        End If
                        If Me._disconnectingState IsNot Nothing Then
                            Me._disconnectingState = Nothing
                        End If
                        If Me._disconnectedState IsNot Nothing Then
                            Me._disconnectedState = Nothing
                        End If
                        If Me._failureState IsNot Nothing Then
                            Me._failureState = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " EVENTS "

        Private eventLocker As Object

        ''' <summary>Raises the Ready event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnReady(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.ReadyEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Ready.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Ready As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Connecting event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnConnecting(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.ConnectingEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine connecting.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Connecting As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Connected event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnConnected(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.ConnectedEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Connected.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Connected As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Disconnecting event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnDisconnecting(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.DisconnectingEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Disconnecting.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Disconnecting As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Disconnected event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnDisconnected(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.DisconnectedEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Disconnected.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Disconnected As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Working event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnWorking(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.WorkingEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Working.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Working As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the WorkDone event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnWorkDone(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.WorkDoneEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine WorkDone.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event WorkDone As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Failure event
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
        ''' </param>
        Protected Overridable Sub OnFailure(ByVal e As System.EventArgs)
            SyncLock eventLocker
                SafeEventHandlers.SafeBeginEndInvoke(Me.FailureEvent, Me, e)
            End SyncLock
        End Sub

        ''' <summary>Occurs upon machine Failure.</summary>
        ''' <remarks>Use this event to notify the container class that the state was activated.</remarks>
        Public Event Failure As EventHandler(Of System.EventArgs)

#End Region

#Region " STATES "

        Private WithEvents _idleState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="IdleState">Idle State</see>.
        ''' </summary>
        Public ReadOnly Property IdleState() As IState
            Get
                Return Me._idleState
            End Get
        End Property

        Private WithEvents _connectingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="ConnectingState">Connecting State</see>.
        ''' </summary>
        Public ReadOnly Property ConnectingState() As IState
            Get
                Return Me._connectingState
            End Get
        End Property

        Private WithEvents _connectedState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="ConnectedState">Connected State</see>.
        ''' </summary>
        Public ReadOnly Property ConnectedState() As IState
            Get
                Return Me._connectedState
            End Get
        End Property

        Private WithEvents _workingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="WorkingState">Working State</see>.
        ''' </summary>
        Public ReadOnly Property WorkingState() As IState
            Get
                Return Me._workingState
            End Get
        End Property

        Private WithEvents _workDoneState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="WorkDoneState">WorkDone State</see>.
        ''' </summary>
        Public ReadOnly Property WorkDoneState() As IState
            Get
                Return Me._workDoneState
            End Get
        End Property

        Private WithEvents _disconnectingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="DisconnectingState">Disconnecting State</see>.
        ''' </summary>
        Public ReadOnly Property DisconnectingState() As IState
            Get
                Return Me._disconnectingState
            End Get
        End Property

        Private WithEvents _disconnectedState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="DisconnectedState">Disconnected State</see>.
        ''' </summary>
        Public ReadOnly Property DisconnectedState() As IState
            Get
                Return Me._disconnectedState
            End Get
        End Property

        Private WithEvents _failureState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="FailureState">Failure State</see>.
        ''' </summary>
        Public ReadOnly Property FailureState() As IState
            Get
                Return Me._failureState
            End Get
        End Property

#End Region

#Region " STATE EVENTS "

        Private Sub _connectingState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectingState.Act
            Me.OnConnecting(System.EventArgs.Empty)
        End Sub

        Private Sub _connectedState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectedState.Act
            Me.OnConnected(System.EventArgs.Empty)
        End Sub

        Private Sub _disconnectingState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _disconnectingState.Act
            Me.OnDisconnecting(System.EventArgs.Empty)
        End Sub

        Private Sub _disconnectedState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _disconnectedState.Act
            Me.OnDisconnected(System.EventArgs.Empty)
        End Sub

        Private Sub _failureState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _failureState.Act
            Me.OnFailure(System.EventArgs.Empty)
        End Sub

        Private Sub _idleState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _idleState.Act
            Me.OnReady(System.EventArgs.Empty)
        End Sub

        Private Sub _workDoneState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _workDoneState.Act
            Me.OnWorkDone(System.EventArgs.Empty)
        End Sub

        Private Sub _workingState_Act(ByVal sender As Object, ByVal e As System.EventArgs) Handles _workingState.Act
            Me.OnWorking(System.EventArgs.Empty)
        End Sub

#End Region

    End Class

    ''' <summary>
    ''' Enumerates the state transition symbols for the 
    ''' <see cref="ConnectWorkDisconnectAutomaton">Connection</see> automaton.
    ''' </summary>
    Public Enum ConnectWorkDisconnectSymbol
        <Description("Not Defined")> None
        <Description("Start Connecting: Idle -> Connecting")> StartConnecting
        <Description("Finish Connecting: Connecting -> Connected")> FinishConnecting
        <Description("Start Working: Idle -> Working")> StartWorking
        <Description("Finish Working: Working -> Work Done")> FinishWorking
        <Description("Start Disconnecting: Idle -> Disconnecting")> StartDisconnecting
        <Description("Finish Disconnecting: Disconnecting -> Disconnected")> FinishDisconnecting
        <Description("Failure: Any State -> Failure")> Failure
        <Description("Idle: .. -> Idle")> Idle
    End Enum

End Namespace
