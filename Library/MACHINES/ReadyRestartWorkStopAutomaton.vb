Imports System.ComponentModel

Namespace Moore

    ''' <summary>
    ''' A base automaton class for implementing a finite automata for starting, restarting, working, and stopping constructs.
    ''' </summary>
    ''' <license>
    ''' (c) 2090 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/26/09" by="David" revision="2.3.3403.x">
    ''' created
    ''' </history>
    Public Class ReadyRestartWorkStopAutomaton
        Inherits Machine(Of ReadyRestartWorkStopSymbol)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New(ByVal restartTimeout As Integer, ByVal startingTimeout As Integer,
                       ByVal startedTimeout As Integer, ByVal stoppingTimeout As Integer)
            MyBase.New()

            MyBase.OnsetDelay = 500
            MyBase.Interval = 1000

            ' define the states.
            Me._readyState = MyBase.AddState("ready")
            Me._failureState = MyBase.AddState("failure")
            Me._failureState.CountOutCount = 3
            Me._restartState = MyBase.AddState("restart", restartTimeout)
            Me._startingState = MyBase.AddState("starting", startingTimeout)
            Me._startedState = MyBase.AddState("Started", startedTimeout)
            Me._workingState = MyBase.AddState("working")
            Me._workDoneState = MyBase.AddState("work_done")
            Me._stoppingState = MyBase.AddState("stopping", stoppingTimeout)
            Me._stoppedState = MyBase.AddState("stopped")

            ' set the start state
            Me.StartStateSetter(Me._readyState)

            ' define the signals.
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Ready)
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Failure)
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Restart)
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Start)
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Finish)
            MyBase.AddSymbol(ReadyRestartWorkStopSymbol.Stop)

            ' define the transitions

            ' READY TRANSITION
            For Each state As IState In MyBase.States.Values
                If state.Equals(Me._readyState) OrElse state.Equals(Me._failureState) OrElse state.Equals(Me._stoppedState) Then
                    MyBase.AddTransition(state, ReadyRestartWorkStopSymbol.Ready, Me._readyState)
                End If
            Next

            ' FAILURE TRANSITION
            For Each state As IState In MyBase.States.Values
                MyBase.AddTransition(state, ReadyRestartWorkStopSymbol.Failure, Me._failureState)
            Next

            MyBase.AddTransition(Me._readyState, ReadyRestartWorkStopSymbol.Start, Me._startingState)
            MyBase.AddTransition(Me._readyState, ReadyRestartWorkStopSymbol.Restart, Me._restartState)

            MyBase.AddTransition(Me._restartState, ReadyRestartWorkStopSymbol.Start, Me._startingState)

            MyBase.AddTransition(Me._startingState, ReadyRestartWorkStopSymbol.Finish, Me._startedState)
            MyBase.AddTransition(Me._startingState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

            MyBase.AddTransition(Me._startedState, ReadyRestartWorkStopSymbol.Start, Me._workingState)
            MyBase.AddTransition(Me._startedState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

            MyBase.AddTransition(Me._workingState, ReadyRestartWorkStopSymbol.Finish, Me._workDoneState)
            MyBase.AddTransition(Me._workingState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

            MyBase.AddTransition(Me._workDoneState, ReadyRestartWorkStopSymbol.Stop, Me._stoppingState)

            MyBase.AddTransition(Me._stoppingState, ReadyRestartWorkStopSymbol.Finish, Me._stoppedState)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._readyState IsNot Nothing Then
                            Me._readyState = Nothing
                        End If
                        If Me._startingState IsNot Nothing Then
                            Me._startingState = Nothing
                        End If
                        If Me._startedState IsNot Nothing Then
                            Me._startedState = Nothing
                        End If
                        If Me._workingState IsNot Nothing Then
                            Me._workingState = Nothing
                        End If
                        If Me._workDoneState IsNot Nothing Then
                            Me._workDoneState = Nothing
                        End If
                        If Me._stoppingState IsNot Nothing Then
                            Me._stoppingState = Nothing
                        End If
                        If Me._stoppedState IsNot Nothing Then
                            Me._stoppedState = Nothing
                        End If
                        If Me._failureState IsNot Nothing Then
                            Me._failureState = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " STATES "

        Private _readyState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="ReadyState">Ready State</see>.
        ''' </summary>
        Public ReadOnly Property ReadyState() As IState
            Get
                Return Me._readyState
            End Get
        End Property

        Private _restartState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="restartState">restart State</see>.
        ''' </summary>
        Public ReadOnly Property RestartState() As IState
            Get
                Return Me._restartState
            End Get
        End Property

        Private _startingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="StartingState">Starting State</see>.
        ''' </summary>
        Public ReadOnly Property StartingState() As IState
            Get
                Return Me._startingState
            End Get
        End Property

        Private _startedState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="StartedState">Started State</see>.
        ''' </summary>
        Public ReadOnly Property StartedState() As IState
            Get
                Return Me._startedState
            End Get
        End Property

        Private _workingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="WorkingState">Working State</see>.
        ''' </summary>
        Public ReadOnly Property WorkingState() As IState
            Get
                Return Me._workingState
            End Get
        End Property

        Private _workDoneState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="WorkDoneState">WorkDone State</see>.
        ''' </summary>
        Public ReadOnly Property WorkDoneState() As IState
            Get
                Return Me._workDoneState
            End Get
        End Property

        Private _stoppingState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="StoppingState">Stopping State</see>.
        ''' </summary>
        Public ReadOnly Property StoppingState() As IState
            Get
                Return Me._stoppingState
            End Get
        End Property

        Private _stoppedState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="StoppedState">Stopped State</see>.
        ''' </summary>
        Public ReadOnly Property StoppedState() As IState
            Get
                Return Me._stoppedState
            End Get
        End Property

        Private _failureState As IState
        ''' <summary>
        ''' Gets reference to the <see cref="FailureState">Failure State</see>.
        ''' </summary>
        Public ReadOnly Property FailureState() As IState
            Get
                Return Me._failureState
            End Get
        End Property

#End Region

    End Class

    ''' <summary>
    ''' Enumerates the state transition symbols for the 
    ''' <see cref="ReadyRestartWorkStopAutomaton">Start</see> automaton.
    ''' </summary>
    Public Enum ReadyRestartWorkStopSymbol
        <Description("Not Defined")> None
        <Description("Restart: Ready to Restart")> Restart
        <Description("Start: Ready, Restart -> Start; Started -> Working; Done Work -> Stopping")> Start
        <Description("Finish: Starting -> Started; Working -> Done Work; Stopping -> Stopped")> Finish
        <Description("Stop: Ready, Restart -> Stopped; Starting, Started -> Stopping")> [Stop]
        <Description("Failure: Any State -> Failure")> Failure
        <Description("Ready: .. -> Ready")> Ready
    End Enum

End Namespace
